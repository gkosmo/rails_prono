 This is another attempt at creating a Prediction Tournament platform. 


## How to play: 

- a User (creator) creates a prediction
- Users validate this prediction 
- If it's validated, Users can then say what are the chances your prediction will happen 
- When the day the prediction passes, then creator say wither the prediction came true or not
- All the users that gave chances to that prediction get their brier score ( the score to how good their prediction are ) updated

## How to run the code 

- Install ruby on rails ( ruby 2.7.1, rails 6.0.2 )
- Install Node ( > 14.0.1 )
- Run in your terminal to install the libs
```ruby 
$ yarn install 
$ bundle install
$ bin/rails db:create db:migrate 
```
- if you want to create seeds `rails db:seed` (the seeds will be coming football matches predictions and 5 users having created varying chances)

- run the server `bin/rails server` 
- login with email: 'a@a.com', password: '123123'


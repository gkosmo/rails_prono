# frozen_string_literal: true

namespace :user_daily do
  desc "TODO"
  task daily_visit: :environment do
    TotalScore.update_all(day_visit: false)
  end
end

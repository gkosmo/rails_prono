namespace :time_check do
  desc "TODO"
  task day_passed: :environment do
    bets = Bet.where(status: Bet::UNVALIDATED)
    bets.each do |bet|
      if (Time.zone.now - bet.created_at).round / 1.day > 7
        tot = bet.validities.count
        tot_up = bet.validities.where(up_vote: true).count
        tot_up > tot / 2 ? bet.update(validated: true, status: Bet::VALID) : bet.update(validated: false, status: Bet::INVALID)
        bet.validities_scores_all
      end
    end

    bets = Bet.where(status: Bet::VALID)
    bets.each do |bet|
      if bet.end_votes_date < Date.today
        bet.update!(status: Bet::DATE_PASSED)
      end
    end
  end
end

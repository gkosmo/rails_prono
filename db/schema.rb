# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_08_30_101424) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "bet_tags", force: :cascade do |t|
    t.bigint "bet_id", null: false
    t.bigint "tag_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bet_id"], name: "index_bet_tags_on_bet_id"
    t.index ["tag_id"], name: "index_bet_tags_on_tag_id"
  end

  create_table "bets", force: :cascade do |t|
    t.date "end_date"
    t.date "end_votes_date"
    t.boolean "validated"
    t.boolean "happened"
    t.boolean "accepted"
    t.string "title"
    t.string "status"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "validities_count"
    t.integer "feelings_count"
    t.index ["user_id"], name: "index_bets_on_user_id"
  end

  create_table "feelings", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "bet_id"
    t.integer "weight_certainty"
    t.boolean "win_feeling"
    t.boolean "win_certainty"
    t.boolean "up_vote"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bet_id"], name: "index_feelings_on_bet_id"
    t.index ["user_id"], name: "index_feelings_on_user_id"
  end

  create_table "scores", force: :cascade do |t|
    t.integer "feelings"
    t.integer "validities"
    t.integer "certainties"
    t.integer "predictions"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "total_brier"
    t.integer "feelings_count"
    t.index ["user_id"], name: "index_scores_on_user_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "total_scores", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "points"
    t.boolean "day_visit"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_total_scores_on_user_id"
  end

  create_table "track_bets", force: :cascade do |t|
    t.bigint "bet_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bet_id"], name: "index_track_bets_on_bet_id"
    t.index ["user_id"], name: "index_track_bets_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "validities", force: :cascade do |t|
    t.boolean "up_vote"
    t.bigint "user_id", null: false
    t.bigint "bet_id"
    t.boolean "win_validity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bet_id"], name: "index_validities_on_bet_id"
    t.index ["user_id"], name: "index_validities_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "bet_tags", "bets"
  add_foreign_key "bet_tags", "tags"
  add_foreign_key "bets", "users"
  add_foreign_key "feelings", "bets"
  add_foreign_key "feelings", "users"
  add_foreign_key "scores", "users"
  add_foreign_key "total_scores", "users"
  add_foreign_key "track_bets", "bets"
  add_foreign_key "track_bets", "users"
  add_foreign_key "validities", "bets"
  add_foreign_key "validities", "users"
end

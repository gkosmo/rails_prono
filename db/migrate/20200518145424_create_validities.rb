class CreateValidities < ActiveRecord::Migration[6.0]
  def change
    create_table :validities do |t|
      t.boolean :up_vote
      t.references :user, null: false, foreign_key: true
      t.references :bet, foreign_key: true
      t.boolean :win_validity

      t.timestamps
    end
  end
end

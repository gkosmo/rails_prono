class CreateScores < ActiveRecord::Migration[6.0]
  def change
    create_table :scores do |t|
      t.integer :feelings
      t.integer :validities
      t.integer :certainties
      t.integer :predictions
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

class CreateFeelings < ActiveRecord::Migration[6.0]
  def change
    create_table :feelings do |t|
      t.references :user, null: false, foreign_key: true
      t.references :bet, foreign_key: true
      t.integer :weight_certainty
      t.boolean :win_feeling
      t.boolean :win_certainty
      t.boolean :up_vote

      t.timestamps
    end
  end
end

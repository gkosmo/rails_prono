class MigrationAddBrierToScores < ActiveRecord::Migration[6.0]
  def change
    add_column :scores, :total_brier, :float
    add_column :scores, :feelings_count, :integer
  end
end

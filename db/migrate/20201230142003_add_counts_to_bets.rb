class AddCountsToBets < ActiveRecord::Migration[6.0]
  def change
    add_column :bets, :validities_count, :integer
    add_column :bets, :feelings_count, :integer
  end
end

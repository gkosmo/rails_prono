class CreateBets < ActiveRecord::Migration[6.0]
  def change
    create_table :bets do |t|
      t.date :end_date
      t.date :end_votes_date
      t.boolean :validated
      t.boolean :happened
      t.boolean :accepted
      t.string :title
      t.string :status
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

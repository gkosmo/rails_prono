class CreateTotalScores < ActiveRecord::Migration[6.0]
  def change
    create_table :total_scores do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :points
      t.boolean :day_visit

      t.timestamps
    end
  end
end

module Seed
  class UsersSeeds
    def self.hundreds
      100.times do
        User.create email: Faker::Internet.email, password: "123123"
      end
      User.all.each do |user|
        user.total_score.update points: 10000000
      end
    end
  end
end

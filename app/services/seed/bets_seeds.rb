# frozen_string_literal: true

module Seed
  class BetsSeeds
    def self.create_bets
      users = [User.first]
      if User.count < 15
        users << User.create(email: "a@a.com", password: "123123")
        users << User.create(email: "c@a.com", password: "123123")
        users << User.create(email: "d@a.com", password: "123123")
        users << User.create(email: "e@a.com", password: "123123")
        Seed::UsersSeeds.hundreds
      end
      30.times do
        team_a = Faker::Team.name
        team_b = Faker::Team.name
        end_date = Faker::Date.in_date_period
        end_vote_date = end_date - 5.days
        a = Bet.new(
          user_id: User.all.sample.id,
          title: "#{end_date.strftime("%Y-%m-%d")} : #{team_a} vs #{team_b} - #{team_a} wins",
          end_date: end_date,
          end_votes_date: end_vote_date,
          validated: false,
          status: Bet::UNVALIDATED
        )

        if a.save
          a.tags.find_or_create_by(name: "football")
          p a
        else
          p a.errors
        end
        fake_votes(a) if a.id
      end
      60.times do
        team_a = Faker::Team.name
        team_b = Faker::Team.name
        end_date = Faker::Date.in_date_period
        end_vote_date = end_date - 5.days
        a = Bet.new(
          user_id: User.all.sample.id,
          title: "#{team_a} vs #{team_b} - #{team_a} wins - on #{end_date.strftime("%Y-%m-%d")} ",
          end_date: end_date,
          end_votes_date: end_vote_date,
          validated: true,
          status: Bet::VALID
        )

        if a.save
          a.tags.find_or_create_by(name: "football")
          p a
        else
          p a.errors
        end
        fake_votes(a) if a.id
        check_bets
      end
    end

    def self.check_bets
      bets = Bet.where(status: Bet::UNVALIDATED)
      bets.each do |bet|
        if (Time.zone.now - bet.created_at).round / 1.day > 7
          tot = bet.validities.count
          tot_up = bet.validities.where(up_vote: true).count
          tot_up > tot / 2 ? bet.update(validated: true, accepted: true, status: Bet::VALID) : bet.update(validated: false, accepted: false, status: Bet::INVALID)
        end
      end

      bets = Bet.where(status: Bet::VALID)
      bets.each do |bet|
        if bet.end_votes_date < Date.today
          bet.update(status: Bet::DATE_PASSED)
        end
      end
    end

    def self.fake_votes(bet)
      dates = (Date.today - 2.years..Date.today).to_a
      User.find_in_batches do |user_array|
        user_array.each do |user|
          next if [true, false].sample
          user.validities.create(bet: bet, up_vote: [false, true].sample, created_at: dates.sample) unless bet.passed_validation

          user.feelings.create(bet: bet, up_vote: [false, true].sample, weight_certainty: (0..100).to_a.sample, created_at: dates.sample) if bet.passed_validation
        end
      end
    end

    def self.createFootballBets(_params = {})
      users = [User.first]
      if User.count < 15
        users << User.create(email: "a@a.com", password: "123123")
        users << User.create(email: "c@a.com", password: "123123")
        users << User.create(email: "d@a.com", password: "123123")
        users << User.create(email: "e@a.com", password: "123123")
        Seed::UsersSeeds.hundreds
      end

      url = "https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php"
      (4328..4359).to_a.each do |league_id|
        sleep (1..15).to_a.sample.seconds
        begin
          call = RestClient.get url, params: {id: league_id}
          next if call.body[0] == "<"

          hash_events = JSON.parse(call.body)["events"]
          next if hash_events.nil?

          transform_call_league(hash_events, "future")
        rescue RestClient::ExceptionWithResponse => e
          p e.response
        end
      end

      url = "https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php"
      (4328..4359).to_a.each do |league_id|
        sleep (1..15).to_a.sample.seconds
        call = RestClient.get url, params: {id: league_id}
        next if call.body[0] == "<"

        hash_events = JSON.parse(call.body)["events"]
        next if hash_events.nil?

        transform_call_league(hash_events, "past")
      rescue RestClient::ExceptionWithResponse => e
        p e.response
      end
    end

    def self.transform_call_league(hash_events, type)
      hash_events.each do |event_hash|
        api_url = "https://www.thesportsdb.com"
        api_id = event_hash["idEvent"]
        date_match = Date.parse(event_hash["dateEvent"])
        end_date_vote = date_match - 1.day
        win_team = (0..9).to_a.sample.odd? ? event_hash["strHomeTeam"] : event_hash["strAwayTeam"]
        end_date_date = date_match
        if type == "future"
          a = Bet.new(
            user_id: User.all.sample.id,
            title: " #{event_hash["strEvent"]} -  #{win_team} wins on #{date_match.to_formatted_s(:short)}",
            end_date: end_date_date,
            end_votes_date: end_date_vote,
            validated: false,
            status: Bet::UNVALIDATED
          )
          if a.save
            a.tags.find_or_create_by(name: "football")
          else
            p a.errors
          end
        else
          a = Bet.new(
            user_id: User.all.sample.id,
            title: "#{event_hash["strEvent"]} - #{win_team} wins on #{date_match.to_formatted_s(:short)}",
            end_date: end_date_date,
            end_votes_date: end_date_vote,
            validated: true,
            status: Bet::VALID
          )

          if a.save
            a.tags.find_or_create_by(name: "football")
          else
            p a.errors
          end
        end
        fake_votes(a) if a.id
      end
    end
  end
end

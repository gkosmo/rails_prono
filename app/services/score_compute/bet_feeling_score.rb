# frozen_string_literal: true

module ScoreCompute
  module BetFeelingScore
    def feelings_scores_all
      feelings.each do |feeling|
        feeling.up_vote == feeling.bet.happened ? score_positive_feeling(feeling) : score_negative_feeling(feeling)
      end
    end

    # Do certainty as well

    def score_positive_feeling(feeling)
      feeling.win_feeling = true
      feeling.user.score.feelings += 1
      feeling.user.score.feelings_count += 1
      feeling.user.score.total_brier += (feeling.weight_certainty / 100.0 - 1)**2
      feeling.user.total_score.points += Totalcore::POINTS_BET_TYPE[:feeling]
      feeling.user.score.save
      feeling.user.total_score.save
      feeling.save
    end

    def score_negative_feeling(feeling)
      feeling.win_feeling = false
      feeling.user.score.feelings -= 1
      feeling.user.score.feelings_count += 1
      feeling.user.score.total_brier += (feeling.weight_certainty / 100.0)**2
      feeling.user.score.save
      feeling.save
    end
  end
end

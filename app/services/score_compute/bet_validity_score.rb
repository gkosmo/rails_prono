# frozen_string_literal: true

module ScoreCompute
  module BetValidityScore
    def validities_scores_all
      validities.each do |validity|
        validity.up_vote == validity.bet.validated ? score_positive_validity(validity) : score_negative_validity(validity)
      end
    end

    # Do certainty as well

    def score_positive_validity(validity)
      validity.win_validity = true
      validity.user.score.validities += 1
      validity.user.total_score.points += TotalScore::POINTS_BET_TYPE[:validity]
      validity.save
    end

    def score_negative_validity(validity)
      validity.win_validity = false
      validity.user.score.validities -= 1
      validity.save
    end
  end
end

import consumer from "./consumer";

const createChannel = (object) => {
  consumer.subscriptions.create(object);
};

export default createChannel;

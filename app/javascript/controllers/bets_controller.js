// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>

import { Controller } from "stimulus"
import Rails from "@rails/ujs";

export default class extends Controller {
  static targets = ["validityInput", "btnFeeling", "feeling"]

  connect() {
    this.id = this.element.dataset.id
  }
  submit_validity(e) {
    let url = `/bets/${this.id}/validities`
    let action = "POST"
    let info = { validity: { up_vote: e.target.dataset.valid } }
    this.sendForm(url, info, action)
  }

  submit_track(e) {
    let url = `/bets/${this.id}/track_bets.json`
    let action = "POST"
    let info = {}

    this.sendForm(url, info, action)
  }

  submit_feeling() {
    let url = `/bets/${this.id}/feelings.json`
    let action = "POST"
    let info = { feeling: { weight_certainty: this.feeling } }
    this.sendForm(url, info, action)
  }

  submit_happening(e) {
    let url = `/bets/${this.id}`
    let action = "PATCH"
    let info = { bet: { status: e.target.dataset.happening } }
    this.sendForm(url, info, action)

  }

  change_feeling(e) {
    let value = e.target.value
    this.feeling = value
    this.feelingTargets.forEach(target => target.innerText = value + "% sure")

    this.btnFeelingTargets.forEach((target) => {
      if (value == 50) {
        target.disabled = true
        target.classList.add("btn-light")
        target.classList.remove("btn-primary")
        target.innerText = "Don't Know Don't Vote"
      } else if (value < 50) {
        target.disabled = false
        target.classList.remove("btn-light")
        target.classList.add("btn-primary")
        target.innerText = "Not happening"
      } else {
        target.disabled = false
        target.classList.remove("btn-light")
        target.classList.add("btn-primary")
        target.innerText = "Happening"
      }
    })
  }


  sendForm = (url, info, action) => {
    fetch(url, {
      method: action,
      credentials: "same-origin",
      headers: {
        "X-CSRF-Token": Rails.csrfToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(info)
    }).then((data) => data.json()).then((data) => {

      if (document.getElementById(`bet-${this.id}`)) {
        document.getElementById(`bet-${this.id}`).outerHTML = data.inner_html;
        //this.removeAlerts()
      }
    });

  }

  removeAlerts() {
    let nodes = document.querySelectorAll(".alert")
    nodes.forEach((node) => {
      setTimeout(() => { node.parentNode.remove() }, 2000)
    })
  }
}

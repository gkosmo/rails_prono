import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = [ "source" ]

  initialize() {
    if (location.hash !== "") {
      this.constructor.openModal(location.hash);
    }
    this.url = `${window.location.pathname}${window.location.search}`;
    this._bindModalEvents();
  }

  static openModal(modalId) {
    $(modalId).modal('show');
  }

  copy(event) {
    event.preventDefault()
    this.sourceTarget.select()
    document.execCommand("copy")
  }

  _updateHash(hash) {
    history.pushState("", document.title, `${this.url}${hash}`);
    this.sourceTarget.value = hash
  }

  _bindModalEvents() {
    $('.modal').on('hidden.bs.modal', () => {
      this._updateHash('');
    });

    $('.modal').on('shown.bs.modal', (e) => {
      if (location.hash !== `#${e.currentTarget.id}`) {
        this._updateHash(`#${e.currentTarget.id}`);
      }
      this.sourceTarget.value = window.location
    });
  }
}

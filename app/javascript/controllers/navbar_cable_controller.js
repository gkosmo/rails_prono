import { Controller } from "stimulus";
import consumer from "../channels/consumer";

export default class extends Controller {
  connect() {
        console.log("controller called")
    console.log(this.element.dataset.totalScoreId)
     this.gameChannel = consumer.subscriptions.create(
      {
        channel: "TotalScoreChannel",
        total_score_id: parseInt(this.element.dataset.totalScoreId),
      },
      {
        received: (data) => {
          console.log("data navbar")
          console.log(data)
          this.element.outerHTML = data.message_partial
          console.log("navbar updated")
       }
      }
    );
  }

  disconnect() {
    if (this.channel) {
      this.channel.unsubscribe();
    }
  }
}

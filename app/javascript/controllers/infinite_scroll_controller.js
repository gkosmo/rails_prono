import { Controller } from "stimulus"
import Rails from "@rails/ujs";

export default class extends Controller {
  static targets = [ "entries", "pagination", "endScroll" ]

  scroll(){
    let url = this.paginationTarget.querySelector("a[rel='next']")
    let body = document.body,
    html = document.documentElement
      console.log("called b4 url")

    var height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight)
    if(window.pageYOffset >= height - window.innerHeight  && url ){
      this.loadMore(url.href)
      console.log("called in url")
    }
  }
  loadMore(url){
    Rails.ajax({
      type: "GET",
      url: url,
      dataType: "json",
      success: (data) => {
        if(data.pagination) {
          console.log("in paginate")
        this.entriesTarget.insertAdjacentHTML("beforeend", data.entries)
        this.paginationTargets.forEach((pagination) => {
        pagination.innerHTML =  data.pagination })
      }
      }
    })
  }
}

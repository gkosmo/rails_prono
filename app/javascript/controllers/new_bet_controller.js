// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>
import flatpickr from "flatpickr";
import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "dateEvent", "dateVote" ]

  connect() {
    if(this.hasDateEventTarget && this.hasDateVoteTarget){
      flatpickr(this.dateEventTarget, {
        minDate: new Date(),
        dateFormat: "d-m-Y",
        onChange: (selectedDates, dateStr, instance)  => {
          this.resetFlatpickrDateVote(selectedDates)
        }
      })
      flatpickr(this.dateVoteTarget, { dateFormat: "d-m-Y"})


    }
  }

  resetFlatpickrDateVote(selectedDates){
    let new_max = new Date(selectedDates[0])
    let new_default = new Date(selectedDates[0])
    console.log(new_max)
    console.log(new_default)
    new_max.setTime(selectedDates[0].getTime() - 1 * 86400000)
    console.log(new_max)

    new_default.setTime(selectedDates[0].getTime() - 2 * 86400000)
    console.log(new_max)
    console.log(new_default)
    flatpickr(this.dateVoteTarget).set({
      maxDate: new_max,
      defaultDate: new_default,
      dateFormat: "d-m-Y"
    })
  }
}

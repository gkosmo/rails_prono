// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>

import { Controller } from "stimulus"


import ApexCharts from 'apexcharts'
window.ApexCharts = ApexCharts


export default class extends Controller {
  static targets = [ "chartFeelings" ]

  connect() {
    let data = JSON.parse(this.chartFeelingsTarget.dataset.set)
    let data_success = JSON.parse(this.chartFeelingsTarget.dataset.setFeelingsSuccess)
    let data_failure = JSON.parse(this.chartFeelingsTarget.dataset.setFeelingsFailure)
    let data_unknown = JSON.parse(this.chartFeelingsTarget.dataset.setFeelingsUnknown)
    var options = {
      series: [{
        name: 'Successes',
        data: data_success
      },{
        name: 'Failures',
        data: data_failure
      },
      {
        name: 'Undetermined yet',
        data: data_unknown
      }],

      title: {
        text: "History of predictions you made",
        align: 'left',
        margin: 10,
        offsetX: 0,
        offsetY: 0,
        floating: false,
        style: {
          fontSize:  '14px',
          fontWeight:  'bold',
          fontFamily:  undefined,
          color:  '#263238'
        },
      },
      chart: {
        height: 350,
        type: 'scatter',
        zoom: {
          type: 'xy'
        }
      },
      dataLabels: {
        enabled: true
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        },
        yaxis: {
          lines: {
            show: true
          }
        },
      },
      xaxis: {
        type: 'datetime',
      },
      yaxis: {
        max: 100
      },
      tooltip: {
       custom: function({series, seriesIndex, dataPointIndex, w}) {
        return '<div class="arrow_box">' +
        '<span>' + data[dataPointIndex]["title"] + '</span>' +
        '<span>' + dataPointIndex + '</span>' +

        '</div>'
      }
    }


  };
  var chart = new ApexCharts(this.chartFeelingsTarget, options);
  chart.render();

}
}

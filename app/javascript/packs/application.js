require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")

import ApexCharts from 'apexcharts'
window.ApexCharts = ApexCharts

import "bootstrap";

import "controllers"

import { initSelect2 } from '../components/init_select2';

initSelect2();



class TotalScoreChannel < ApplicationCable::Channel
  def subscribed
    stream_from "total_score_#{params[:total_score_id]}"
    # stream_from "some_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end

class ScoreFeelingsJob < ApplicationJob
  queue_as :default

  def perform(bet_id)
    bet = Bet.find(bet_id)
    bet.feelings_scores_all
    # Do something later
  end
end

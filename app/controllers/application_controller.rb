class ApplicationController < ActionController::Base
  before_action :essentials
  def essentials
    if current_user
      current_user&.total_score&.check_daily_visit
      @tracked_bets = current_user&.tracked_bets
    end
  end
end

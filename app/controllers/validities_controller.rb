class ValiditiesController < ApplicationController
  before_action :authenticate_user!

  def create
    bet = Bet.find params[:bet_id]
    validity = current_user.validities.new(validities_params)
    validity.bet = bet
    if validity.save
      card_string = render_to_string(partial: "bets/bet", locals: {bet: bet.reload})
      render json: {inner_html: card_string}, status: 200
    else
      card_string = render_to_string(partial: "bets/bet", locals: {bet: bet.reload, validity: validity})
      render json: {inner_html: card_string}, status: 200
    end
  end

  private

  def validities_params
    params.require(:validity).permit(:up_vote)
  end
end

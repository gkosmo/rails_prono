class PagesController < ApplicationController
  before_action :authenticate_user!, except: [:home, :rules]

  def home
  end

  def dashboard
    @current_user_bets = current_user.bets.includes(:tags, :validities, :feelings, user: :tracked_bets).references(:tags)
    @current_user_feelings_bets = current_user.feeling_bets.includes(:tags, :validities, :feelings, user: :tracked_bets).references(:tags)
    @current_user_feelings = current_user.feelings.includes(:bet)
    @current_user_validities_bets = current_user.validity_bets.includes(:tags, :validities, :feelings, user: :tracked_bets).references(:tags)
  end
end

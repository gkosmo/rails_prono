class TrackBetsController < ApplicationController
  before_action :authenticate_user!

  def create
    bet = Bet.find(params[:bet_id])
    if current_user.tracked_bets.include? bet
      @tracking = current_user.track_bets.find_by(bet: bet)
      @tracking.destroy
    else
      @tracking = current_user.track_bets.create(bet: bet)
    end
    card_string = render_to_string(partial: "bets/bet.html.erb", locals: {bet: bet.reload, feeling: Feeling.new, validity: Validity.new}, formats: :html)
    render json: {inner_html: card_string}, status: 200
  end
end

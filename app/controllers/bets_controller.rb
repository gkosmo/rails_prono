class BetsController < ApplicationController
  include Pagy::Backend
  before_action :authenticate_user!, except: [:index]
  before_action :set_bet, only: [:show, :edit, :update, :destroy]
  before_action :reset_filters, only: :index

  # GET /bets
  # GET /bets.json
  def index
    @bets = Bet.list_bets(@list_params, current_user)
    @pagy, @bets = pagy(@bets, items: 30)
    entries_string = render_to_string(partial: "bets/bets", locals: {bets: @bets})
    pagination_string = view_context.pagy_nav(@pagy)
    respond_to do |format|
      format.html
      format.json {
        render json: {
          entries: entries_string,
          pagination: pagination_string
        }
      }
    end
  end

  # GET /bets/1
  # GET /bets/1.json
  def show
  end

  # GET /bets/new
  def new
    @bet = Bet.new
  end

  # GET /bets/1/edit
  def edit
  end

  # POST /bets
  # POST /bets.json
  def create
    @bet = Bet.new(bet_params)
    @bet.user = current_user
    respond_to do |format|
      if @bet.save
        tags_params[:tag_names].each { |t_name| @bet.tags << Tag.find_or_create_by!(name: t_name) }
        format.html { redirect_to dashboard_path, notice: "Bet was successfully created." }
        format.json { render :show, status: :created, location: @bet }
      else
        format.html { render :new }
        format.json { render json: @bet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bets/1
  # PATCH/PUT /bets/1.json
  def update
    redirect_to bets_path unless @bet.user == current_user
    @bet.update(bet_params)
    card_string = render_to_string(partial: "bets/card.html.erb", locals: {bet: @bet.reload}, formats: :html)
    render json: {inner_html: card_string}, status: 200
  end

  # DELETE /bets/1
  # DELETE /bets/1.json
  def destroy
    @bet.destroy
    respond_to do |format|
      format.html { redirect_to bets_url, notice: "Bet was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  def reset_filters 
    @list_params = params[:bet_query].present? ? params[:bet_query].deep_dup : {}
    if @list_params[:reset_filter].present?
      @list_params.delete(:status)
      @list_params.delete(:sort_by)
      params[:bet_query][:sort_by] = nil if params.dig(:bet_query, :sort_by)
      params[:bet_query][:status] = nil if params.dig(:bet_query, :status)

      @list_params.delete(:reset_filter)
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_bet
    @bet = Bet.find(params[:id])
  end

  def tags_params
    params.require(:tag).permit(tag_names: [])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bet_params
    params.require(:bet).permit(:title, :status, :end_date, :end_votes_date, :validated, :happened)
  end
end

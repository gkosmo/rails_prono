class FeelingsController < ApplicationController
  before_action :authenticate_user!

  def create
    bet = Bet.find params[:bet_id]
    feeling = current_user.feelings.new(bet_feeling_params)
    feeling.up_vote = feeling.weight_certainty > 50
    feeling.bet = bet
    if feeling.save
      card_string = render_to_string(partial: "bets/bet.html.erb", locals: {bet: bet.reload, feeling: feeling}, formats: :html)
      render json: {inner_html: card_string}, status: 200
    else
      card_string = render_to_string(partial: "bets/bet.html.erb", locals: {bet: bet.reload, feeling: feeling}, formats: :html)
      render json: {inner_html: card_string}, status: 200
    end
  end

  private

  def bet_feeling_params
    params.require(:feeling).permit(:weight_certainty)
  end
end

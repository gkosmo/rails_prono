class Validity < ApplicationRecord
  belongs_to :user
  belongs_to :bet, counter_cache: true
  validates_uniqueness_of :bet_id, scope: :user_id
  after_create :update_score
  after_initialize :init
  def init
    self.win_validity ||= false
  end

  validate :total_score_allows

  private

  def total_score_allows
    if user.total_score.points < TotalScore::POINTS_BET_TYPE[:validity]
      errors.add(:user, "Doesnt have enough points ")
    end
  end

  def update_score
    user.total_score.points -= TotalScore::POINTS_BET_TYPE[:validity]
    user.total_score.save
  end
end

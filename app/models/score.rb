class Score < ApplicationRecord
  belongs_to :user
  validates :user, uniqueness: true

  after_initialize :init

  def init
    self.feelings ||= 0
    self.validities ||= 0
    self.certainties ||= 0
    self.predictions ||= 0
    self.total_brier ||= 0
    self.feelings_count ||= 0
  end

  def brier_score
    feelings_count.zero? ? 0.5 : (total_brier / feelings_count).round(2)
  end
end

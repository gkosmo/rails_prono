class Feeling < ApplicationRecord
  belongs_to :bet, counter_cache: true
  belongs_to :user
  validates_uniqueness_of :bet_id, scope: :user_id
  # validates :weight_certainty, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  # validates :up_vote, inclusion: { in: [true, false] }
  after_create :update_score

  validate :total_score_allows

  private

  def total_score_allows
    if user.total_score.points < TotalScore::POINTS_BET_TYPE[:feeling]
      errors.add(:user, "Doesnt have enough points ")
    end
  end

  def update_score
    user.total_score.points -= TotalScore::POINTS_BET_TYPE[:feeling]
    user.total_score.save
  end
end

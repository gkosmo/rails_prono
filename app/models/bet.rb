class Bet < ApplicationRecord
  include ::ScoreCompute::BetFeelingScore
  include ::ScoreCompute::BetValidityScore
  include PgSearch::Model

  pg_search_scope :search_by_title, against: [:title], associated_against: {tags: :name}

  STATI = [
    UNVALIDATED = "UNVALIDATED",
    INVALID = "INVALID",
    VALID = "VALID",
    DATE_PASSED = "DATE_PASSED",
    HAPPENED = "HAPPENED",
    NOT_HAPPENED = "NOT_HAPPENED",
  ]

  STATI_DISPLAY = [
    ["unvalidated yet", UNVALIDATED],
    ["invalid", INVALID],
    ["valid", VALID],
    ["date passed", DATE_PASSED],
    ["event has happened", HAPPENED],
    ["event hasn't happened", NOT_HAPPENED],
  ]

  BET_FEELING_VOTED = [
    BET_FEELING_VOTED_NOT_YET = "BET_FEELING_VOTED_NOT_YET",
    BET_FEELING_VOTED_FOR = "BET_FEELING_VOTED_FOR",
    BET_FEELING_VOTED_AGAINST = "BET_FEELING_VOTED_AGAINST"
  ]

  BET_VALIDITY_VOTED = [
    BET_VALIDITY_VOTED_NOT_YET = "BET_VALIDITY_VOTED_NOT_YET",
    BET_VALIDITY_VOTED_AGAINST = "BET_VALIDITY_VOTED_AGAINST",
    BET_VALIDITY_VOTED_FOR = "BET_VALIDITY_VOTED_FOR"
  ]

  VOTES_FILTER = [
    BET_FEELING_VOTED,
    BET_VALIDITY_VOTED,
    BETS_TRACKED = "BETS_TRACKED"
  ].flatten

  VOTES_FILTER_DISPLAY = [
    ["bets not voted for yet", BET_FEELING_VOTED_NOT_YET],
    ["bets you've voted for", BET_FEELING_VOTED_FOR],
    ["bets you've voted against", BET_FEELING_VOTED_AGAINST],
    ["bets not you haven't validated yet", BET_VALIDITY_VOTED_NOT_YET],
    ["bets you've unvalidated", BET_VALIDITY_VOTED_AGAINST],
    ["bets you've validated", BET_VALIDITY_VOTED_FOR],
    ["bets you're tracking", BETS_TRACKED],
  ]

  SORT_FILTER = [
    CREATED_AT_ASC = "CREATED_AT_ASC",
    CREATED_AT_DESC = "CREATED_AT_DESC",
    NUMBER_VALIDITIES_ASC = "NUMBER_VALIDITIES_ASC",
    NUMBER_VALIDITIES_DESC = "NUMBER_VALIDITIES_DESC",
    NUMBER_FEELINGS_ASC = "NUMBER_FEELINGS_ASC",
    NUMBER_FEELINGS_DESC = "NUMBER_FEELINGS_DESC"
  ]
  SORT_FILTER_DISPLAY = [
    ["descending by creation", CREATED_AT_DESC],
    ["ascending by creation", CREATED_AT_ASC],
    ["most valid to least valid", NUMBER_VALIDITIES_ASC],
    ["least valid to most valid", NUMBER_VALIDITIES_DESC],
    ["most amount of bets to least amount", NUMBER_FEELINGS_ASC],
    ["least amount of bets to most amount", NUMBER_FEELINGS_DESC]
  ]

  belongs_to :user
  has_many :validities, dependent: :nullify
  has_many :feelings, dependent: :nullify
  has_many :bet_tags
  has_many :tags, through: :bet_tags

  validates :title, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: {in: STATI}
  validates :end_date, presence: true
  validates :end_votes_date, presence: true
  validate :total_score_allows
  validate :end_votes_date_after_end_date

  after_update :process_feelings, if: :saved_change_to_status?
  after_create :update_score

  attr_accessor :user_voted_for, :points

  after_initialize :init

  def init
    points = {}
    points ||= points
    self.status ||= UNVALIDATED
  end

  def validity_voted_for(user)
    validities.detect { |validity| validity.user_id == user.id }
  end

  def feeling_voted_for(user)
    feelings.detect { |feeling| feeling.user_id == user.id }
  end

  def feelings_average
    if feelings.size > 0
      feelings.sum { |feeling| feeling.weight_certainty }.div(feelings.size)
    else
      0
    end
  end

  def process_feelings
    ScoreFeelingsJob.perform_later(id) if [HAPPENED, NOT_HAPPENED].include?(status)
  end

  def passed_validation
    self.status != UNVALIDATED
  end

  def validated
    [VALID, DATE_PASSED, HAPPENED, NOT_HAPPENED].include?(status)
  end

  def self.list_bets(params, user)
    bets = includes(:user, :tags, validities: [:user], feelings: [:user], user: :tracked_bets).where(nil)
    votes_filter = params.delete(:votes_filter) if params[:votes_filter].present?
    return user.tracked_bets if votes_filter&.include? Bet::BETS_TRACKED
    votes_search = params.delete(:search) if params[:search].present?
    votes_sort = params.delete(:sort_by) if params[:sort_by].present?
    params.each do |key, value|
      bets = bets.where(key => value) if value.present?
    end
    bets = vote_filter(votes_filter, bets, user) if votes_filter
    bets = bets.search_by_title(votes_search) if votes_search
    bets = bets.sorting_by(votes_sort, bets) if votes_sort
    bets
  end

  private

  def self.sorting_by(votes_sort, bets)
    case votes_sort
    when CREATED_AT_DESC
      bets.order(created_at: :asc)
    when CREATED_AT_DESC
      bets.order(created_at: :desc)
    when NUMBER_FEELINGS_ASC
      bets.order("feelings_count asc")
    when NUMBER_FEELINGS_DESC
      bets.order("feelings_count desc")
    when NUMBER_VALIDITIES_ASC
      bets.order("validities_count asc")
    when NUMBER_VALIDITIES_DESC
      bets.order("validities_count desc")
    else
      bets
    end
  end

  def self.vote_filter(votes_filter, bets, user)
    votes_filter.delete("")
    if votes_filter.include? Bet::BET_VALIDITY_VOTED_NOT_YET
      bets = bets.where.not(id: user.validities.map(&:bet_id))
    end
    if votes_filter.include? Bet::BET_VALIDITY_VOTED_FOR
      bets = bets.where(id: user.validities.where(up_vote: true).map(&:bet_id))
    end
    if votes_filter.include? Bet::BET_VALIDITY_VOTED_AGAINST
      bets = bets.where(id: user.validities.where(up_vote: false).map(&:bet_id))
    end

    if votes_filter.include? Bet::BET_FEELING_VOTED_NOT_YET
      bets = bets.where.not(id: user.feelings.map(&:bet_id))
    end
    if votes_filter.include? Bet::BET_FEELING_VOTED_FOR
      bets = bets.where(id: user.feelings.where(up_vote: true).map(&:bet_id))
    end
    if votes_filter.include? Bet::BET_FEELING_VOTED_AGAINST
      bets = bets.where(id: user.feelings.where(up_vote: false).map(&:bet_id))
    end

    bets
  end

  def total_score_allows
    if status == "UNVALIDATED" && user.total_score.points < TotalScore::POINTS_BET_TYPE[:bet]
      errors.add(:user, "Doesnt have enough points ")
    end
  end

  def end_votes_date_after_end_date
    if end_date < end_votes_date
      errors.add(:end_votes_date, "The end date can't be after the event date")
    end
  end

  def update_score
    user.total_score.points -= TotalScore::POINTS_BET_TYPE[:bet]
    user.total_score.save
  end
end

class TotalScore < ApplicationRecord
  POINTS_BET_TYPE = {
    validity: 0,
    day_bet: 2,
    feeling: 3,
    bet: 4
  }.freeze

  belongs_to :user
  validates :user, uniqueness: true
  after_commit :broadcast_score
  after_initialize :init

  def init
    self.points ||= 20
  end

  def check_daily_visit
    unless day_visit
      self.day_visit = true
      self.points += 20
      save
    end
  end

  private

  def broadcast_score
    ActionCable.server.broadcast("total_score_#{id}", {
      message_partial: ApplicationController.renderer.render(
        partial: "shared/navbar",
        locals: {current_user: user}
      ),
      current_user_id: user.id
    })
  end
end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  has_many :validities
  has_many :feelings
  has_one :score
  has_one :total_score
  has_many :bets

  has_many :feeling_bets, through: :feelings, source: :bet
  has_many :validity_bets, through: :validities, source: :bet

  has_many :track_bets
  has_many :tracked_bets, through: :track_bets, source: :bet

  before_save :init

  def init
    self.total_score ||= TotalScore.new
    self.score ||= Score.new
    true
  end
end

class Tag < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  downcase_before_validation_for :name
  strip_attributes
end

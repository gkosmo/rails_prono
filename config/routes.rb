Rails.application.routes.draw do
  devise_for :users
  resources :bets do
    resources :validities, only: [:create]
    resources :feelings, only: [:create]
    resources :track_bets, only: [:create, :destroy]
  end
  root to: "bets#index"
  get :dashboard, to: "pages#dashboard"
  get :rules, to: "pages#rules"
  require "sidekiq/web"
  authenticate :user, ->(user) { user.admin? } do
    mount Sidekiq::Web => "/sidekiq"
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
